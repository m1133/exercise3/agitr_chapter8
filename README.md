Session 5: Communications using services (https://sir.upc.edu/projects/rostutorials)

Exercise 3a: Make a copy of pubvel_toggle_plus.cpp and name it improved_pubvel_toggle.cpp. Modify this new file to include:
1. A service to start/stop the turtle.
2. A service that allows to change the speed.

Exercise 3b: Make a copy of spawn_turtle.cpp and name it spawn_turtle_plus.cpp. Modify this new file in order that, besides calling the /spawn service to create a new turtle called MyTurtle, this node also:

1. Subscribes to the topic turtle1/cmd_vel (that will be provided by the pubvel_toggle_plus node).
2. Publishes the received command velocity to the topic MyTurtle/cmd_vel in order to move both of the turtles simultaneously.
