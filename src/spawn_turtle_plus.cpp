// This program spawns a new turtlesim turtle by calling
// the appropriate service.
#include <ros/ros.h>
// The srv class for the service.
#include <turtlesim/Spawn.h>
#include <geometry_msgs/Twist.h>

ros::Publisher* pubPtr;

// callback function when subscribing topic
void callback(const geometry_msgs::Twist& msg)
{
    geometry_msgs::Twist move;
    move.linear.x = msg.linear.x;
    move.angular.z = msg.angular.z;
    pubPtr->publish(move);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "spawn_turtle_plus");
    ros::NodeHandle nh;

    // Initialize publish and subscribe object
    pubPtr = new ros::Publisher(nh.advertise<geometry_msgs::Twist>("MyTurtle/cmd_vel", 1000));
    ros::Subscriber sub = nh.subscribe("turtle1/cmd_vel", 1000, &callback);

    // Create a client object for the spawn service. This
    // needs to know the data type of the service and its name.
    ros::ServiceClient spawnClient = nh.serviceClient<turtlesim::Spawn>("spawn");

    // Create the request and response objects.
    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x = 5.544445;
    req.y = 3.544445;
    req.theta = 0;  // M_PI/2;
    req.name = "MyTurtle";

    ros::service::waitForService("spawn", ros::Duration(5));
    bool success = spawnClient.call(req, resp);

    if (success) {
        ROS_INFO_STREAM("Spawned a turtle named " << resp.name);
    } else {
        ROS_ERROR_STREAM("Failed to spawn.");
    }

    ros::spin();

    delete pubPtr;
}
