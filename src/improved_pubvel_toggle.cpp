// this program toggles between rotation and translation
// commands,based on calls to a service.
#include <ros/ros.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>
#include <agitr_chapter8_plus/Changerate.h>
#include <agitr_chapter8_plus/ChangeSpeed.h>

bool forward = true;
double newfrequency = 2;
bool start = true;
double newspeed_x = 1.0;
double newspeed_z = 1.0;

bool toggleForward(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
    forward = !forward;
    ROS_INFO_STREAM("Now sending " << (forward ? "forward" : "rotate") << " commands.");
    return true;
}

bool start_stop_turtle(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
    start = !start;
    ROS_INFO_STREAM((start ? "Started" : "Stoped") << " the turtle.");
    return true;
}

bool changeRate(agitr_chapter8_plus::Changerate::Request& req, agitr_chapter8_plus::Changerate::Response& resp)
{
    ROS_INFO_STREAM("Changing rate to " << req.newrate);

    newfrequency = req.newrate;
    return true;
}

bool changeSpeed(agitr_chapter8_plus::ChangeSpeed::Request& req, agitr_chapter8_plus::ChangeSpeed::Response& resp)
{
    ROS_INFO_STREAM("Changing speed x to " << req.x << ", speed z to" << req.z);

    newspeed_x = req.x;
    newspeed_z = req.z;
    return true;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "improved_pubvel_toggle");
    ros::NodeHandle nh;

    ros::ServiceServer server = nh.advertiseService("toggle_forward", &toggleForward);
    ros::ServiceServer server0 = nh.advertiseService("change_rate", &changeRate);
    ros::ServiceServer server1 = nh.advertiseService("start_stop", &start_stop_turtle);
    ros::ServiceServer server2 = nh.advertiseService("change_speed", &changeSpeed);

    ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);

    ros::Rate rate(newfrequency);
    while (ros::ok()) {
        geometry_msgs::Twist msg;

        if (start) {
            msg.linear.x = forward ? newspeed_x : 0.0;
            msg.angular.z = forward ? 0.0 : newspeed_z;
        } else {
            msg.linear.x = 0.0;
            msg.angular.z = 0.0;
        }

        pub.publish(msg);
        ros::spinOnce();

        rate = ros::Rate(newfrequency);

        rate.sleep();
    }
}
